# import the necessary packages
import os
from imutils import contours
import numpy as np
import argparse
import imutils
import cv2
from skimage.feature import match_template
from matplotlib import pyplot as plt, patches
import xlsxwriter

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--imagedir", required=True,
                help="path to the input images directory")

ap.add_argument("-r", "--reference", required=True,
                help="path to the reference image to extract box contour and locations")

ap.add_argument("-n", "--identity_reference", required=True,
                help="path to the reference image to extract identity informations")
args = vars(ap.parse_args())

# hardcoded value obtained on reference image
IDENTITY_ROI = ((20, 130), (500, 55))
AGE_ROI = ((130, 180), (80, 55))
CHOICE_LETTERS = ("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l")


def load_image(path_to_image_file):
    """
    Loads an image using cv2
    :param path_to_image_file:
    :return:
    """
    image = cv2.imread(path_to_image_file)
    return image


def load_image_and_apply_filters(path_to_image):
    """
    Loads the image and apply simpe filters
    :param path_to_image:
    :return:
    """
    image = load_image(path_to_image)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (5, 5), 0)

    thresh = cv2.threshold(gray, 0, 255,
        cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

    return thresh


def extract_tickbox_contours():
    """
    Finds the  contour of the tickboxes in the reference (not box ticked) image
    """

    thresh = load_image_and_apply_filters(args["reference"])
    # find contours in the thresholded image, then initialize
    # the list of contours that correspond to questions
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    questionCnts = []
    # We sort the contours top to bottom
    cnts = contours.sort_contours(cnts, method="top-to-bottom")[0]

    # loop over the contours
    for c in cnts:
        # compute the bounding box of the contour, then use the
        # bounding box to derive the aspect ratio
        (x, y, w, h) = cv2.boundingRect(c)
        ar = w / float(h)

        # in order to label the contour as a question, region
        # should be sufficiently wide, sufficiently tall, and
        # have an aspect ratio approximately equal to 1
        if 17 >= w >= 12 and 17 >= h >= 12 and 0.9 <= ar <= 1.1 and x < 140 and y > 400:
            questionCnts.append((c, (x, y, w, h)))

    x_list = [contour[1][0] for contour in questionCnts]
    w_max = max([contour[1][2] for contour in questionCnts])
    h_max = max([contour[1][3] for contour in questionCnts])

    y_min = questionCnts[0][1][1]
    y_max = questionCnts[-1][1][1]

    x_min = min(x_list)
    x_max = max(x_list)

    return questionCnts, thresh, (x_min, x_max, y_min, y_max, w_max, h_max)


def get_choices(image, tickbox_contours, template, x_offset, y_offset):
    """
    Knowing the tickbox_contours, returns the choices ticked.
    Choice is determine by maximum density for each consecutive 4 tickboxes
    :param path_to_image:
    :param tickbox_contours:
    :return:
    """

    # We correct the shift between ref and current image using match template
    result = match_template(image, template)
    ij = np.unravel_index(np.argmax(result), result.shape)
    x_template, y_template = ij[::-1]
    delta_x = x_template-x_offset
    delta_y = y_template-y_offset

    print(f"Found tickboxes column at offset {delta_x}, {delta_y}")

    choices = []
    for offset in (0, 4, 8):
        contours = tickbox_contours[offset:offset+4]
        scores = []
        for contour in contours:
            c, (x, y, w, h) = contour
            scores.append(np.sum(image[y+delta_y:y+delta_y+h,
                                 x+delta_x:x+delta_x+w]))
        choices.append(np.argmax(scores)+offset)
    return choices, delta_x, delta_y


def get_identity(raw_image, image, identity_template, identity_roi, age_roi):
    """
    Returns a 2-tuple (identity_string, age_string) and (identity_np_array, age_np_array).
    This function first finds the offset of the identity template,
    extracts the identity and age ROI and uses a OCR library to convert
    these two hand written information intro strings.
    Also Returns the 2-tuple np array of the extracted ROI for human validation.
    :param image: cv2 image
    :param identity_template: identity template cv2 image
    :param identity_roi: ((x_offset, y_offset), (width, height)) of the identity ROI
    :param age_roi: ((x_offset, y_offset), (width, height)) of the age ROI
    :return: (identity_img, age_img)
    """
    (id_x_offset, id_y_offset), (id_width, id_height) = identity_roi
    (age_x_offset, age_y_offset), (age_width, age_height) = age_roi

    result = match_template(image, identity_template)
    ij = np.unravel_index(np.argmax(result), result.shape)
    x_template, y_template = ij[::-1]

    template_in_image = raw_image[y_template:y_template+identity_template.shape[0],
               x_template: x_template + identity_template.shape[1]]

    id_image = template_in_image[id_y_offset: id_y_offset+id_height,
               id_x_offset: id_x_offset+id_width]

    age_image = template_in_image[age_y_offset: age_y_offset+age_height,
               age_x_offset: age_x_offset+age_width]

    return id_image, age_image


def save_result_png(image, choices, box_pos, path_to_png):
    """
    Makes a png highlighting where we found the ticked boxes
    :param path_to_png:
    :return:
    """
    plt.clf()
    ax = plt.subplot(111)
    plt.imshow(image, cmap='gray')
    for i, tick_box in enumerate(box_pos):
        # Create a Rectangle patch
        if i in choices:
            (c, (x, y, w, h)) = tick_box
            rect = patches.Rectangle((x+delta_x, y+delta_y), w, h, linewidth=0.5, edgecolor='r', facecolor='none')

            # Add the patch to the Axes
            ax.add_patch(rect)
            ax.text(x+delta_x, y+delta_y, CHOICE_LETTERS[i], color="red")

    plt.savefig(path_to_png, dpi=600)
    plt.close()


box_pos, gray, (x_min, x_max, y_min, y_max, w_max, h_max) = extract_tickbox_contours()
assert len(box_pos) == 12, "Expecting 12 tickboxes"
template = gray[y_min:y_max+h_max, x_min:x_max+w_max]
identity_template = load_image_and_apply_filters(args["identity_reference"])

workbook = xlsxwriter.Workbook(args["imagedir"]+'.xlsx',
                               {'constant_memory': True})

worksheet = workbook.add_worksheet("extraction_automatique")

row_id = 0
worksheet.write_row(row_id, 0, ("Nom de Fichier", "Directives Anticipées",
                                "Personne de Confiance", "Don d'organes",
                                "Image Nom", "Image Age", "Nom", "Age", "Sexe"))
row_id += 1

worksheet.set_column(0, 3, 20)
worksheet.set_column(4, 4, 100)
worksheet.set_column(5, 5, 20)
worksheet.set_column(6, 7, 20)

counter = 0
# for filename in os.listdir(args["imagedir"]):
#     full_path = os.path.join(args["imagedir"], filename)
#     if filename.endswith(".png") and "-result" not in filename and "-age" not in filename and "-id" not in filename:
#         file_id = int(filename.rsplit(".", maxsplit=1)[0].rsplit("-", maxsplit=1)[1])

imagedir = args["imagedir"]
imageprefix = os.path.split(imagedir)[1]
print(f"dirname: {imageprefix}")

while True:
    filename = imageprefix + f"-{counter}.png"
    full_path = os.path.join(imagedir, filename)

    if not os.path.exists(full_path):
        print(f"Stopped after {counter} entries on {full_path}")
        break

    print(f"Processing {filename}")

    raw_image = load_image(full_path)
    raw_image = cv2.rotate(raw_image, cv2.ROTATE_90_CLOCKWISE)

    image = load_image_and_apply_filters(full_path)
    image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)

    id_image, age_image = get_identity(raw_image, image,
                                       identity_template, IDENTITY_ROI, AGE_ROI)

    id_image_path = os.path.join(args["imagedir"], filename + "-id.png")
    cv2.imwrite(id_image_path, id_image)

    age_image_path = os.path.join(args["imagedir"], filename + "-age.png")
    cv2.imwrite(age_image_path, age_image)

    next_page = full_path.replace(f"-{counter}.png", f"-{counter+1}.png")
    image = load_image_and_apply_filters(next_page)
    image = cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)

    choices, delta_x, delta_y = get_choices(image, box_pos, template, x_min, y_min)
    filename = filename.split(".")[0]
    save_result_png(image, choices, box_pos,
                    os.path.join(args["imagedir"], filename + "-result.png"))

    data = [filename] + [CHOICE_LETTERS[x] for x in choices]
    worksheet.write_row(row_id, 0, data)
    worksheet.insert_image(row_id, 4, id_image_path, {'positioning': 1,
                                                      'x_scale': 1, 'y_scale': 1})

    worksheet.insert_image(row_id, 5, age_image_path, {'positioning': 1, 'x_scale': 1,
                                                       'y_scale': 1})
    worksheet.set_row(row_id, max(id_image.shape[0], age_image.shape[0]))
    row_id += 1
    counter += 2

workbook.close()
