convert_lizou() {
    # Converts all pdfs of a given directory into pngs
    pushd `pwd`
    echo "cd into target directory $1"
    cd $1
    # find . -name "*.pdf" -exec convert -density 150 {} -quality 90 test.png \;
    shopt -s globstar nullglob
    lizou_pdfs=( ./*".pdf"* )
    printf '%s\n' "${lizou_pdfs[@]}"
    for i in "${lizou_pdfs[@]}"
    do
        fname=$(basename ${i})
        dirname=${fname%.*}
        mkdir ${dirname}
        echo "Converting ${fname} into ${dirname}.png"
        convert -density 150 "${fname}" -quality 90 "${dirname}/${dirname}.png"
    done
    popd
}